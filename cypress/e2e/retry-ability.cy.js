describe("Retry-ability", () => {
    beforeEach(() => {
        cy.createEvent();
    });
    it("should create an event with the correct name", () => {
        cy.get("h2").should("have.text", "Event Details")

        cy.get("ul li")
            .should("have.length", 7)
            .find("span.value")
            .should("contain", "Sample Event");
    });
});