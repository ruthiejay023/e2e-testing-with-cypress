describe("Mixing Synchronous and Asynchronous Code", () => {
    beforeEach(() => {
        cy.createEvent();
    });
    it("should navigate to events list after cancelling an event", () => {
       
        let isCancelled = false;
        cy.get("#cancel-event-btn").click().then(() => {
            isCancelled = true;

            cy.get("h2").should("have.text", "Events List");

            if (isCancelled) {
                cy.go("back");
            }
        });

        cy.get("h2").should("have.text", "Event Details");
    });
});
